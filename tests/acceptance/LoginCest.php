<?php namespace App\Tests;
use App\Tests\AcceptanceTester;

class LoginCest
{
    private $loginURL = '/login';
    private $accountOverviewURL = '/en/account-overview/';

    private $usernameField = 'form input[name=userIdentifier]';
    private $passwordField = 'form input[name=password]';
    private $passwordDropdown = 'Password';
    private $submitButton = 'form button[type=submit]';

    private $registeredEmail = '';
    private $unregisteredEmail = '';
    private $invalidEmail = '';
    private $validPassword = '';
    private $invalidPassword = '';

    public function _before(AcceptanceTester $I)
    {
    }

    // tests
    public function successfulLoginTest(AcceptanceTester $I)
    {
        $this->goToLoginPage($I);
        $I->fillField($this->usernameField, $this->registeredEmail);
        $I->click($this->submitButton);
        $I->waitForElement($this->passwordField);
        $I->click($this->passwordDropdown);
        $I->wait(1);
        $I->fillField($this->passwordField, $this->validPassword);
        $I->click($this->submitButton);
        $I->wait(10);
        $I->seeCurrentUrlEquals($this->accountOverviewURL);
    }

    public function invalidEmailTest(AcceptanceTester $I) {
        $this->goToLoginPage($I);
        $I->fillField($this->usernameField, $this->invalidEmail);
        $I->click($this->submitButton);
        $I->waitForText('The specified user could not be found');
    }

    public function unregisteredEmailTest(AcceptanceTester $I)
    {
        $this->goToLoginPage($I);
        $I->fillField($this->usernameField, $this->unregisteredEmail);
        $I->click($this->submitButton);
        $I->waitForText('The specified user could not be found');
    }

    public function invalidPasswordTest(AcceptanceTester $I) {
        $I->fillField($this->usernameField, $this->registeredEmail);
        $I->click($this->submitButton);
        $I->waitForElement($this->passwordField);
        $I->click($this->passwordDropdown);
        $I->fillField($this->passwordField, $this->invalidPassword);
        $I->click($this->submitButton);
        $I->waitForText('Incorrect password. Please try again.');
    }

    private function goToLoginPage(AcceptanceTester $I) {
        $I->amOnPage($this->loginURL);
        $I->waitForElement($this->usernameField);
    }
}
