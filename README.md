TESTER
--

**Requirements:**
- PHP 7 or higher CLI
- Java (For Selenium)
- Selenium Server Standalone (http://docs.seleniumhq.org/download/)
- ChromeDriver (https://sites.google.com/a/chromium.org/chromedriver/downloads) or GeckoDriver (https://github.com/mozilla/geckodriver)

**Installation instructions:**
1. cd <this_project_path>
2. composer install
3. open `tests/acceptance/LoginCest.php` define parameters for `$registeredEmail`, `$unregisteredEmail`, `$invalidEmail`, `$validPassword`, `$invalidPassword`
4. run all test `vendor/bin/codecept run acceptance`

**Run Test Scenarios:**
- Successful Login: `vendor/bin/codecept run acceptance LoginCest:successfulLoginTest`
- Invalid Email: `vendor/bin/codecept run acceptance LoginCest:invalidEmailTest`
- Unregistered Email: `vendor/bin/codecept run acceptance LoginCest:unregisteredEmailTest`
- Invalid Password: `vendor/bin/codecept run acceptance LoginCest:invalidPasswordTest`